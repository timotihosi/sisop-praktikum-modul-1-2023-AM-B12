#!/bin/bash
# login retep.sh

users_list="users/users.txt"

function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'LOGIN:' $1 $2" >> log.txt
}

function main {
  echo "Login System"
  echo "Masukan Username dan Password"

  read -p "Username: " username
  read -p "Password: " password

  # mengecek apakah username terdaftar
  if [[ ! "$(grep "$username" "$users_list")" ]] ; then
    	echo -e "\nUsername atau password salah"
    exit 1
  fi

  # mengecek password
  if [[ "$(grep "$username" "$users_list" | cut -d ':' -f 2)" == "$password" ]] ; then
    	echo -e "\nLogin berhasil"
    	log "INFO" "User $username logged in"
  else
    	echo -e "\nLogin gagal"
	log "ERROR" "Failed attempt on user $username"
    exit 1
  fi
}

main
