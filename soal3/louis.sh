#!/bin/bash
# register louis.sh

#apabila pertama mengenerate maka akan membuat dir
mkdir -p "users"

users_list="users/users.txt"

function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'REGISTER:' $1 $2" >> log.txt
}

function main {

  echo "Registration System"
  echo "Masukan username dan password baru"

  read -p "Username: " username

  read -p "Password: " password

  # mengecek apakah username sudah terdaftar pada file users.txt
  if [[ "$(grep "$username" "$users_list")" ]]; then
    	echo -e "\nUsername sudah terdaftar"
    	log "ERROR" "User already exists"
    exit 1
  fi

  # minimal 8 karakter
  if [[ "${#password}" -lt 8 ]]; then
    	echo -e "\nPassword minimal 8 karakter"
    exit 1
  fi

  # memiliki minimal 1 huruf kapital dan 1 huruf kecil
  if [[ ! $(echo "$password" | grep '[a-z]' | grep '[A-Z]') ]]; then
    	echo -e "\nPassword harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    exit 1
  fi

  # alphanumeric
  if [[ "$password" =~ [^a-zA-Z0-9] ]]; then
    	echo -e "\nPassword harus menggunakan karakter alphanumeric"
    exit 1
  fi

  # tidak boleh sama dengan username
  if [[ "$password" == "$username" ]]; then
    	echo -e "\nPassword tidak boleh sama dengan username"
    exit 1
  fi

  # tidak boleh menggunakan kata chicken atau ernie
  if [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]]; then
    	echo -e "\nPassword tidak boleh menggunakan kata chicken atau ernie"
    exit 1
  fi

  echo "$username:$password" >> "$users_list"
  echo -e "\nAkun telah dibuat"
  log "INFO" "User $username registered succesfully"
}

main
