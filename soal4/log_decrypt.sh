#!/bin/bash

read -p "Insert file name: " encrypted_filename

current_hour=$(echo "$encrypted_filename" | cut -d ':' -f1)

mkdir -p backup_decrypt

encrypted_filepath="/home/timothy/sisop/prak1nomer4/backup_encrypt/${encrypted_filename}"
decrypted_filepath="/home/timothy/sisop/prak1nomer4/backup_decrypt/${encrypted_filename}"


uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')
lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')

if [ -f "$encrypted_filepath" ]; then
  cat "${encrypted_filepath}" | tr "${lowercase_alphabet:${current_hour}:26}${uppercase_alphabet:${current_hour}:26}" "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}" > "${decrypted_filepath}"
fi
