#!/bin/bash

current_date=$(date "+%H:%M %d:%m:%Y")
current_hour=$(date "+%H")

mkdir -p backup_encrypt

backup_filename="/home/timothy/sisop/prak1nomer4/backup_encrypt/${current_date}.txt"

uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')
lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')

cat /var/log/syslog | tr "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}" "${lowercase_alphabet:${current_hour}:26}${uppercase_alphabet:${current_hour}:26}" > "${backup_filename}"

#Cron Job ( run every 2 hours)	
#0 */2 * * * /bin/bash /home/timothy/sisop/prak1nomer4/log_encrypt.sh
