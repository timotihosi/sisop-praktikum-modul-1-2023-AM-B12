#!/bin/bash

# mendapatkan jam sekarang
current_hour=$(date +"%H")

# jika jam sekarang adalah 00:00, maka download 1 gambar
if [ $current_hour -eq 0 ]; then
  num_images=1
else
  num_images=$current_hour
fi

#membuat folder baru
folder_name="/home/timothy/sisop/prak1nomer2/kumpulan_$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1+1}')"
mkdir $folder_name

#mendownload gambar sebanyak x kali
for (( i=1; i<=$num_images; i++ ))
do
	filename="perjalanan_$i.file"
	wget -O "$folder_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
done

# Crontab
# 0 */10 * * * /bin/bash /home/timothy/sisop/prak1nomer2/kobeni_liburan.sh

let zip_count=$(ls -d devil_* | wc -l)+1
nama_zip="devil_$zip_count"

jumlahfolder=$(ls -d kumpulan_* | wc -l)
 

let jumlahfolder2=$jumlahfolder-1 
let jumlahfolder3=$jumlahfolder2-1 
if [ $(expr $jumlahfolder % 5) == 3 ]
then
    zip -r $nama_zip kumpulan_$jumlahfolder kumpulan_$jumlahfolder2 kumpulan_$jumlahfolder3
elif [ $(expr $jumlahfolder % 5) == 0 ]
then
    zip -r $nama_zip kumpulan_$jumlahfolder kumpulan_$jumlahfolder2
fi

# berdasarkan pola file akan men zip tiap 3 file lalu 2 file lalu 3 file dst.
