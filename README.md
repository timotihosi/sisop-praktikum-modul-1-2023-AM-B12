# sisop-praktikum-modul-1-2023-AM-B12

## Group Member    :
| Nama                              | NRP        |
|-----------------------------------|------------|
|Timothy Hosia Budianto             |5025211098  |
|Arif Nugraha Santosa               |5025211048  |
|Abdullah Yasykur Bifadhlil Midror  |5025211035  |
|Abdurrohim Usaamah Khafizduddin    |5025201255  |

berikut adalah demo untuk praktikum 1

## Nomor 1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi

#### Soal A
Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. **Tampilkan 5 Universitas dengan ranking tertinggi di Jepang**.

**Penyelesaian :**
```sh
awk -F, '$3 == "JP"' '2023 QS World University Rankings.csv' | sort -t"," -nk1 | head -n5 | awk -F, '{print $1, $2, "of", $4, $9}'
```
**Penjelasan :**
- ``awk`` merupakan sebuah program yang bisa digunakan untuk mengambil catatan/record tertentu dalam sebuah file dan melakukan sebuah beberapa operasi terhadap catatan/record tersebut.
- ``-F,`` merupakan command ``awk`` yang digunakan sebagai separator antar kolom (dalam kasus ini, file .csv dipisahkan oleh koma).
- ``$3 == "JP"``  merupakan comman ``awk`` yang akan mengambil semua data yang mengandung kata JP pada kolom ketiga.
- ``2023 QS World University Rankings.csv`` merupakan file yang berisi data data universitas.
- ``sort -t"," -nk1`` merupakan command linux yang akan melakukan sorting, pada hal ini yang akan disort adalah kolom pertama dari data yang ada secara ascending di sintaks``-nk1`` (Kolom ke-1 berisi rank).
- ``head -n5`` merupakan command untuk mengambil data teratas sebanyak n, dalam hal ini ia akan mengambil 5 data teratas.

#### Soal B 
Karena Bocchi kurang percaya diri dengan kemampuannya, coba **cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang**. 

**Penyelesaian :**
```sh
awk -F, '$3 == "JP"' '2023 QS World University Rankings.csv' | sort -t"," -nk9 | head -n5 | awk -F, '{print $1, $2, "of", $4, $9}'
```
**Penjelasan :**
- ``awk`` merupakan sebuah program yang bisa digunakan untuk mengambil catatan/record tertentu dalam sebuah file dan melakukan sebuah beberapa operasi terhadap catatan/record tersebut.
- ``-F,`` merupakan command ``awk`` yang digunakan sebagai separator antar kolom (dalam kasus ini, file .csv dipisahkan oleh koma).
- ``$3 == "JP"``  merupakan comman ``awk`` yang akan mengambil semua data yang mengandung kata JP pada kolom ketiga.
- ``2023 QS World University Rankings.csv`` merupakan file yang berisi data data universitas.
- ``sort -t"," -nk9`` merupakan command linux yang akan melakukan sorting, pada hal ini yang akan disort adalah kolom ke-9 dari data yang ada secara ascending di sintaks``-nk9`` (Kolom ke-9 berisi fsr score).
- ``head -n5`` merupakan command untuk mengambil data teratas sebanyak n, dalam hal ini ia akan mengambil 5 data teratas.

#### Soal C 
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, **cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi**.

**Penyelesaian :**
```sh
awk -F, '$3 == "JP"' '2023 QS World University Rankings.csv' | sort -t"," -nk20 | head -n10 | awk -F, '{print $1, $2, "of", $4, $20}'
```
**Penjelasan :**
- ``awk`` merupakan sebuah program yang bisa digunakan untuk mengambil catatan/record tertentu dalam sebuah file dan melakukan sebuah beberapa operasi terhadap catatan/record tersebut.
- ``-F,`` merupakan command ``awk`` yang digunakan sebagai separator antar kolom (dalam kasus ini, file .csv dipisahkan oleh koma).
- ``$3 == "JP"``  merupakan comman ``awk`` yang akan mengambil semua data yang mengandung kata JP pada kolom ketiga.
- ``2023 QS World University Rankings.csv`` merupakan file yang berisi data data universitas.
- ``sort -t"," -nk20`` merupakan command linux yang akan melakukan sorting, pada hal ini yang akan disort adalah kolom ke-20 dari data yang ada secara ascending di sintaks``-nk20`` (Kolom ke-20 berisi ger rank).
- ``head -n10` merupakan command untuk mengambil data teratas sebanyak n, dalam hal ini ia akan mengambil 10 data teratas.

#### Soal D 
Bocchi ngefans berat dengan universitas paling keren di dunia. **Bantu bocchi mencari universitas tersebut dengan kata kunci keren**.

**Penyelesaian :**
```sh
awk -F, '/Keren/ {print $1, $2, "of", $4}' '2023 QS World University Rankings.csv'
```
**Penjelasan :**
- Command ``awk -F,`` dan ``print`` sudah dijelaskan pada subnomor di atas.
- Command ``\Keren\`` pada ``awk`` digunakan untuk mencari data yang mengandung kata "Keren".

**Kesulitan Nomor 1 :**
- _Tidak ada._

**Output**
| <p align="center"> university_survey.sh </p> |
| -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-1-2023-AM-B12/-/raw/main/Images/no1.png" width = "400"/> |

## Nomor 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 

#### Soal A

Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst)

**Penyelesaian dan penjelasan :**
```sh
#!/bin/bash

# mendapatkan jam sekarang
current_hour=$(date +"%H")

# jika jam sekarang adalah 00:00, maka download 1 gambar
if [ $current_hour -eq 0 ]; then
  num_images=1
else
  num_images=$current_hour
fi
```
Pertama kita menyimpan data jam sekarang dalam variable yang diberi nama current_hour.
Lalu kita masuk dalam sebuah if-case, jika jam sekarang adalah 00:00, variable num_images hanya akan menyimpan nilai 1, jika tidak
num_images hanya akan menyimpan nilai sebanyak jam sekarang yang disimpan dalam variabel current_hour.

```sh
#membuat folder baru
folder_name="/home/timothy/sisop/prak1nomer2/kumpulan_$(ls | grep -c '^kumpulan_[0-9]*$' | awk '{print $1+1}')"
mkdir $folder_name

#mendownload gambar sebanyak x kali
for (( i=1; i<=$num_images; i++ ))
do
	filename="perjalanan_$i.file"
	wget -O "$folder_name/$filename" "https://source.unsplash.com/1600x900/?indonesia"
done
```
Setelah itu kita membuat variable folder_name dimana dia akan mempertimbangkan ada berapa folder dengan nama kumpulan_, kemudian menambahkannya dengan 1. Hal ini dilakukan untuk memastikan folder baru yang kita buat akan memiliki nama yang berurutan secara increment.

Lalu kita akan masuk dalam perulangan for sebanyak num_images yang sudah di tentukan diatas tadi, dimana kita akan mendowload gambar indonesia sebanyak num_images dari sumber internet "https://source.unsplash.com/1600x900/?indonesia" dengan comand ``wget``. Selain itu penamaan file adalah perjalanan_i , dimana i adalah hitungan iterasi for.
```sh
# Crontab
# 0 */10 * * * /bin/bash /home/timothy/sisop/prak1nomer2/kobeni_liburan.sh
```

Terakhir akan dilakukan cronjob agar file dijalankan tiap 10 jam dari pertama kali di run. Syntaxnya seperti diatas, dimana setelah ``0 */10 * * *`` adalah bash direktori file kita sekarang yaitu ``/home/timothy/sisop/prak1nomer2/kobeni_liburan.sh``

#### Soal B

Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.

**Penyelesaian dan Penjelasan :**
```sh
let zip_count=$(ls -d devil_* | wc -l)+1
nama_zip="devil_$zip_count"

jumlahfolder=$(ls -d kumpulan_* | wc -l)

```
Pertama kita buat dan isi variabel nama_zip dengan variable zip_count. Variable zip_count akan menghitung jumlah zip yang ada dan menambahkannya dengan 1 sama seperti pada variable kumpulan_ diatas. Kemudian dengan metode yang sama kita akan menghitung jumlah folder kumpulan yang ada dan disimpan dalam variable jumlahfolder.

```sh
let jumlahfolder2=$jumlahfolder-1 
let jumlahfolder3=$jumlahfolder2-1 
if [ $(expr $jumlahfolder % 5) == 3 ]
then
    zip -r $nama_zip kumpulan_$jumlahfolder kumpulan_$jumlahfolder2 kumpulan_$jumlahfolder3
elif [ $(expr $jumlahfolder % 5) == 0 ]
then
    zip -r $nama_zip kumpulan_$jumlahfolder kumpulan_$jumlahfolder2
fi

# berdasarkan pola file akan men zip tiap 3 file lalu 2 file lalu 3 file dst.
```
Berdasarkan pola, kita dapat melihat bahwa penzip-an file akan dilakukan setiap 3 file dan 2 file secara berurutan yaitu 3-2-3-2-3-2 dst. Oleh karena itu kita akan membuat variable jumlahfolder2 dan 3 dimana akan menyimpan nilai -1 dan -2 dari jumlahfolder. Setelah itu kita akan masuk dalam sebuah if-else. Jika jumlah folder di modulo 5 menghasilkan 3, berarti kita akan memasukan 3 folder kumpulan yang terakhir dibuat kedalam 1 file zip. Sedangkan, Jika jumlah folder di modulo 5 menghasilkan 0, berarti kita akan memasukan 2 folder kumpulan yang terakhir dibuat kedalam 1 file zip. File zip akan diberi nama_zip sesuai yang sudah dijelaskan diatas tadi.

**Kesulitan Nomor 2 :**
- _Melakukan zipping menggunakan crontab._

**Output**
| <p align="center"> kobeni_liburan.sh </p> |
| ------------------------------------------|
| <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-1-2023-AM-B12/-/raw/main/Images/zipno2.jpg" width = "400"/> |

## Nomor 3
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

#### Soal A
Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username 
- Tidak boleh menggunakan kata chicken atau ernie

**Penyelesaian :** 
```sh
#!/bin/bash
# register louis.sh

#apabila pertama mengenerate maka akan membuat dir
mkdir -p "users"

users_list="users/users.txt"
```
**Penjelasan  :**
- ``mkdir -p`` untuk membuat direktori baru dengan mengabaikan error jika direktori sudah ada sebelumnya.
- ``users_list`` adalah variabel yang berisi path dari file yang digunakan untuk menyimpan username dan password terdaftar yaitu ``user.txt``.
```sh
function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'REGISTER:' $1 $2" >> log.txt
}
```
**Penjelasan  :**
- fungsi ``log``yang akan digunakan untuk menyetorkan timestamp dari user register menuju file ``log.txt``.
```sh
function main {

  echo "Registration System"
  echo "Masukan username dan password baru"

  read -p "Username: " username

  read -p "Password: " password

  # mengecek apakah username sudah terdaftar pada file users.txt
  if [[ "$(grep "$username" "$users_list")" ]]; then
    	echo -e "\nUsername sudah terdaftar"
    	log "ERROR" "User already exists"
    exit 1
  fi

  # minimal 8 karakter
  if [[ "${#password}" -lt 8 ]]; then
    	echo -e "\nPassword minimal 8 karakter"
    exit 1
  fi

  # memiliki minimal 1 huruf kapital dan 1 huruf kecil
  if [[ ! $(echo "$password" | grep '[a-z]' | grep '[A-Z]') ]]; then
    	echo -e "\nPassword harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    exit 1
  fi

  # alphanumeric
  if [[ "$password" =~ [^a-zA-Z0-9] ]]; then
    	echo -e "\nPassword harus menggunakan karakter alphanumeric"
    exit 1
  fi

  # tidak boleh sama dengan username
  if [[ "$password" == "$username" ]]; then
    	echo -e "\nPassword tidak boleh sama dengan username"
    exit 1
  fi

  # tidak boleh menggunakan kata chicken atau ernie
  if [[ $(echo "$password" | grep -i 'chicken') ]] || [[ $(echo "$password" | grep -i 'ernie') ]]; then
    	echo -e "\nPassword tidak boleh menggunakan kata chicken atau ernie"
    exit 1
  fi

  echo "$username:$password" >> "$users_list"
  echo -e "\nAkun telah dibuat"
  log "INFO" "User $username registered succesfully"
}

main
```

**Penjelasan :**
- fungsi ``main`` berisi input username password, mengecek username terdaftar, dan validasi password.
- ``echo -e`` untuk menampilkan output dengan tambahan ``\`` yang pada script diatas berupa ``\n``.
- ``grep`` berfungsi untuk mencari string atau pola tertentu pada file text yang telah dibuat.
- ``-lt`` adalah operator less than, sama seperti `<=`.
- ``grep -i`` untuk mencari string/pola dengan mengabaikan case sensitive.
- statement ``log`` berfungsi memanggil fungsi ``log`` dengan input argumen berada di dalam ``""``.

#### Soal B
Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.
- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
-  Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

**Penyelesaian :**
```sh
#!/bin/bash
# login retep.sh

users_list="users/users.txt"

function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'LOGIN:' $1 $2" >> log.txt
}

function main {
  echo "Login System"
  echo "Masukan Username dan Password"

  read -p "Username: " username
  read -p "Password: " password

  # mengecek apakah username terdaftar
  if [[ ! "$(grep "$username" "$users_list")" ]] ; then
    	echo -e "\nUsername atau password salah"
    exit 1
  fi

  # mengecek password
  if [[ "$(grep "$username" "$users_list" | cut -d ':' -f 2)" == "$password" ]] ; then
    	echo -e "\nLogin berhasil"
    	log "INFO" "User $username logged in"
  else
    	echo -e "\nLogin gagal"
	log "ERROR" "Failed attempt on user $username"
    exit 1
  fi
}

main
```

**Penjelasan :**
- ``cut -d ':' -f 2`` berfungsi untuk mengekstrak kolom kedua (yaitu password) pada file ``user.txt`` untuk dibandingkan dengan variabel ``$password`` yang diinput user saat login. Jika sesuai, maka menampilkan "Login berhasil".
- opsi ``-d ':'`` digunakan untuk menentukan tanda titik dua (:) sebagai delimiter, sehingga perintah ``cut`` akan memotong input menjadi kolom-kolom berdasarkan tanda titik dua.

**Kesulitan Nomor 3 :**
- _Tidak ada._

**Output**
| <p align="center"> hasil user.txt dan log.txt </p> |
| -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-1-2023-AM-B12/-/raw/main/Images/no3.png" width = "600"/> |

## Nomor 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan :

1. Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt)
2. Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
- Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
- Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya
- Setelah huruf z akan kembali ke huruf a
3. Buat juga script untuk dekripsinya
4. Backup file syslog setiap 2 jam untuk dikumpulkan

**Penyelesaian dan penjelasan :**

#### Encrypt
```sh
#!/bin/bash

current_date=$(date "+%H:%M %d:%m:%Y")
current_hour=$(date "+%H")

mkdir -p backup_encrypt

backup_filename="/home/timothy/sisop/prak1nomer4/backup_encrypt/${current_date}.txt"

uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')
lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')

```
Pertama-tama kita mengambil data tanggal dan jam dan disimpan divariable current_date dan current_hour. lalu kita buat folder backup_encrypt untuk menyimpan file-file hasil dari enkripsi kita . Setelah itu kita buat variable backup_filename/current_date di dalam folder backup_encrypt untuk menyimpan hasil dari enkripsi kita.

Variable uppercase_alphabet dan lowercase_alphabet akan menyimpan alphabet kapital dan non kapital dari A - Z yang diulang 2 kali agar bisa di maju dan mundurkan saat melakukan transform.

```sh
cat /var/log/syslog | tr "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}" "${lowercase_alphabet:${current_hour}:26}${uppercase_alphabet:${current_hour}:26}" > "${backup_filename}"

#Cron Job ( run every 2 hours)	
#0 */2 * * * /bin/bash /home/timothy/sisop/prak1nomer4/log_encrypt.sh
```
Terakhir kita akan mengambil syslog dengan syntax ``cat /var/log/syslog`` lalu mentransform semua huruf kapital dan non-kapital dari syslog dengan dimajukan sejumlah current_hour dan dimsukan ke file backup_filename.

Setelah itu akan dilakukan Cron job untuk me run file log_encrypt.sh tiap 2 jam.

#### Decrypt

```sh
read -p "Insert file name: " encrypted_filename

current_hour=$(echo "$encrypted_filename" | cut -d ':' -f1)

mkdir -p backup_decrypt

encrypted_filepath="/home/timothy/sisop/prak1nomer4/backup_encrypt/${encrypted_filename}"
decrypted_filepath="/home/timothy/sisop/prak1nomer4/backup_decrypt/${encrypted_filename}"
```

Pertama kita akan meminta user untuk memasukan nama file yang akan di Decrypt dan dimasukan ke dalam variale encrypted_filename. Setelah itu variabel current_hour akan mengambil nilai jam yang tersimpan dalam nama file, dengan cara mengambil first word dan dibatasi tanda titik 2 dengan syntax ``cut -d ':' -f1``.

Kemudian kita akan membuat folder backup_decrypt untuk menyimpan file hasil Decrypt. Lalu kita akan membuat file path encrypted_filepath dan decrypted_filepath untuk menyimpan direktori file yang akan dienkripsi dan hasil Decrypt.

```sh
uppercase_alphabet=$(echo -e 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ')
lowercase_alphabet=$(echo -e 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz')

if [ -f "$encrypted_filepath" ]; then
  cat "${encrypted_filepath}" | tr "${lowercase_alphabet:${current_hour}:26}${uppercase_alphabet:${current_hour}:26}" "${lowercase_alphabet:0:26}${uppercase_alphabet:0:26}" > "${decrypted_filepath}"
fi
```

Setelah itu kita akan menyimpan uppercase_alphabet dan lowercase_alphabet dengan tujuan sama seperti di file log_encrypt. Terkahir kita masuk kedalam if-else, dimana jika encrypted_filepath adalah true dan ditemukan, kita akan mengambil isi file nya kemudian di transform dimajukan lagi sejauh current_hour untuk kembali ke posisi semula, dan di simpan dalam file decrypted_filepath.

**Kesulitan Nomor 4 :**
- _Tidak ada._

**Output**
| <p align="center"> log_encrypt.sh </p> | <p align="center"> log_decrypt.sh </p> |
| -------------------------------------------- | -------------------------------------------- |
| <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-1-2023-AM-B12/-/raw/main/Images/no4encrypt.jpg" width = "400"/> | <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-1-2023-AM-B12/-/raw/main/Images/no4decrypt.jpg" width = "400"/> |`
| <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-1-2023-AM-B12/-/raw/main/Images/no4encryptisi.jpg" width = "400"/> | <img src="https://gitlab.com/timotihosi/sisop-praktikum-modul-1-2023-AM-B12/-/raw/main/Images/no4decryptisi.jpg" width = "400"/> |
