#!/bin/bash

#1a
echo "Soal 1a"
awk -F, '$3 == "JP"' '2023 QS World University Rankings.csv' | sort -t"," -nk1 | head -n5 | awk -F, '{print $1, $2, "of", $4, $9}'
echo

#1b
echo "Soal 1b"
awk -F, '$3 == "JP"' '2023 QS World University Rankings.csv' | sort -t"," -nk9 | head -n5 | awk -F, '{print $1, $2, "of", $4, $9}'
echo

#1c
echo "Soal 1c"
awk -F, '$3 == "JP"' '2023 QS World University Rankings.csv' | sort -t"," -nk20 | head -n10 | awk -F, '{print $1, $2, "of", $4, $20}'
echo

#1d
echo "Soal 1d"
awk -F, '/Keren/ {print $1, $2, "of", $4}' '2023 QS World University Rankings.csv'
echo

